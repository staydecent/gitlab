# frozen_string_literal: true

resource :subscriptions, only: [:new]
